﻿using MemberApi.Security.Filters;
using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;

namespace MemberApi.Controllers
{
    [JwtAuthentication]
    public class TokenController : ApiController
    {
        // GET: Token
        [HttpGet]
        [AllowAnonymous]
        public IHttpActionResult GetToken(string username, string password)
        {
            var accountServices = new AccountServices();
            var token = accountServices.GetToken(username, password);
            if(token != null)
            {
                return Ok(token);
            }
            return Unauthorized();
        }
    }
}