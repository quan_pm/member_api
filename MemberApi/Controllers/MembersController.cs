﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using DBServices;
using MemberApi.Security.Filters;
using Services;
using Transport;

namespace MemberApi.Controllers
{
    [JwtAuthentication]
    public class MembersController : ApiController
    {
        // GET: api/Members/5
        [HttpGet]
        [ResponseType(typeof(Member))]
        public IHttpActionResult GetMember(Guid id)
        {
            var services = new MemberServices();
            var member = services.GetMember(id);
            if (member == null)
            {
                return NotFound();
            }

            return Ok(new { member.Id, member.Name, member.Email, member.MobileNumber, member.Gender, member.DateOfBirth, member.EmailOptIn});
        }

        // PUT: api/Members/5
        [HttpPut]
        [ResponseType(typeof(void))]
        public IHttpActionResult PutMember(MemberUpdateModel member)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (member.Id == Guid.Empty)
            {
                return BadRequest();
            }

            try
            {
                var services = new MemberServices();
                var result = services.UpdateMember(member);
                if(!result)
                {
                    return StatusCode(HttpStatusCode.ExpectationFailed);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return StatusCode(HttpStatusCode.OK);
        }

        // POST: api/Members
        [HttpPost]
        [AllowAnonymous]
        [ResponseType(typeof(Member))]
        public IHttpActionResult PostMember([FromBody]Member member)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                var services = new MemberServices();
                member = services.CreateMember(member);
                return CreatedAtRoute("DefaultApi", new { id = member.Id }, member);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}