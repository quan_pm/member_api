﻿using DBServices;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Transport;

namespace Services
{
    public class MemberServices
    {
        private EntityDbContext db { get; set; }
        public MemberServices()
        {
            db = new EntityDbContext();
        }

        public Member GetMember(Guid id)
        {
            return db.Members.Find(id);
        }

        public Member CreateMember(Member member)
        {
            if (MemberExists(member))
            {
                throw new Exception("Member already exist");
            }
            if (member.Id == Guid.Empty)
                member.Id = Guid.NewGuid();
            member.Password = Utils.MD5Encrypt(member.Password);
            db.Members.Add(member);
            db.SaveChanges();
            return member;
        }

        public bool UpdateMember(MemberUpdateModel member)
        {
            var memberToUpdate = GetMember(member.Id);
            if(memberToUpdate == null)
            {
                return false;
            }
            memberToUpdate.Name = member.Name;
            memberToUpdate.MobileNumber = member.MobileNumber;
            memberToUpdate.Gender = member.Gender;
            memberToUpdate.DateOfBirth = member.DateOfBirth;
            memberToUpdate.EmailOptIn = member.EmailOptIn;
            db.Entry(memberToUpdate).State = EntityState.Modified;
            db.SaveChanges();
            return true;
        }
        private bool MemberExists(Member member)
        {
            var list = db.Members.Where(m => m.Id == member.Id || m.Email == member.Email).ToList();
            return list.Any();
        }

        public bool CheckUser(string email, string password)
        {
            var encodedPassword = Utils.MD5Encrypt(password);
            if(db.Members.Any(m=>m.Email == email && m.Password == encodedPassword))
            {
                return true;
            }
            return false;
        }
    }
}
