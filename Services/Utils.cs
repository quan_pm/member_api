﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Services
{
    public class Utils
    {
        public static string MD5Encrypt(string rawText)
        {
            byte[] asciiBytes = ASCIIEncoding.ASCII.GetBytes(rawText);
            byte[] hashedBytes = MD5CryptoServiceProvider.Create().ComputeHash(asciiBytes);
            string hashedString = BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
            return hashedString;
        }

        public static void Log(string message)
        {
            var fileName = $@"log_{DateTime.Today}.txt";
            var filePath = Path.Combine(@"D:\", fileName);
            if(File.Exists(filePath))
            {
                File.AppendText(message);
            }
        }
    }
}
