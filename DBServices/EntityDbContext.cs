﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBServices
{
    public class EntityDbContext : DbContext
    {
        public EntityDbContext() : base()
        {

        }

        public DbSet<Member> Members { get; set; }
    }
}
